function filterCollection(collection, keywords, matchAll, ...fields) {
  const searchTerms = keywords.toLowerCase().split(' ');

  return collection.filter((item) => {
    const objectMatches = fields.some((field) => {
      const fieldValue = deepValue(item, field);
      if (fieldValue) {
        const lowercaseValue = fieldValue.toString().toLowerCase();
        return searchTerms.some((term) => lowercaseValue.includes(term));
      }
      return false;
    });

    return matchAll ? objectMatches : objectMatches.length > 0;
  });
}

function deepValue(obj, path) {
  const parts = path.split('.');
  for (const part of parts) {
    obj = obj[part];
  }
  return obj;
}

const vehicles = [
  {
    name: 'Toyota Camry',
    description: 'A reliable car from Toyota.',
    contentType: { name: 'Sedan' },
    locales: [
      { name: 'en_US', description: 'Toyota vehicle in the US' },
    ],
  },
  {
    name: 'Honda Civic',
    description: 'A popular car from Honda.',
    contentType: { name: 'Sedan' },
    locales: [
      { name: 'en_US', description: 'Honda vehicle in the US' },
      { name: 'en_DE', description: 'BMW vehicle in Germany' },
    ],
  },
];

const filteredVehicles = filterCollection(vehicles, 'en_us toyota', true, 'name', 'description', 'contentType.name', 'locales.name', 'locales.description');
console.log(filteredVehicles);
